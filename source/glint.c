/*
** glint.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sat Apr 12 17:19:52 2014 charti
** Last update Fri Jun  6 09:30:32 2014 charti
*/

#include "rt.h"
#include "iobj.h"

/*
** Calculte the Glinted Vector
*/
static t_mlx_coord	get_glint_vect(t_mlx_coord vect, t_mlx_coord normal)
{
  double		dot_prod;
  t_mlx_coord		glint_vect;

  dot_prod = get_dot_product(vect, normal) * (-2);
  glint_vect.x = vect.x + (normal.x * dot_prod);
  glint_vect.y = vect.y + (normal.y * dot_prod);
  glint_vect.z = vect.z + (normal.z * dot_prod);
  return (glint_vect);
}

static void	update_glint(t_iobj *near, t_color *color, t_color *obj_color)
{
  int		c;

  c = -1;
  while (++c < 4)
    color->argb[c] = color->argb[c] * (1 - near->glint)
      + obj_color->argb[c] * near->glint;
}

/*
** Calculate Glint
*/
t_color		set_glint(t_scene scene, t_origin org,
			  t_color color, t_mlx_coord pt)
{
  t_mlx_coord	glint_vect;
  t_iobj	*near;
  t_iobj	*obj;
  t_color	obj_color;
  int		c;

  near = scene.ls_obj.near;
  glint_vect = get_glint_vect(org.rot, near->get_normal(near, pt));
  scene.eye = set_origin(pt, glint_vect);
  if ((obj = get_near_obj(scene.ls_obj, scene.eye, near)))
    {
      scene.ls_obj.near = obj;
      obj_color = set_light(scene,
			    get_pt(obj->dist, set_origin(pt, glint_vect)));
      update_glint(near, &color, &obj_color);
    }
  else
    {
      c = -1;
      while (++c < 4)
  	color.argb[c] = color.argb[c] * (1 - near->glint);
    }
  return (color);
}
