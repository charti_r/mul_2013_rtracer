/*
** light.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Thu Apr 10 11:14:53 2014 charti
** Last update Sun Jun  8 17:48:17 2014 charti
*/

#include "rt.h"
#include "iobj.h"

static void	update_light_color(t_iobj *near, t_light *light,
				   unsigned int argb[], t_mlx_coord pt)
{
  t_mlx_coord	light_vect;
  t_mlx_coord	norm;
  double	cos;
  int		c;

  light_vect = get_vect(light->pos, pt);
  norm = near->get_normal(near, pt);
  cos = cos_vect(light_vect, norm);
  if (cos > 0.0)
    {
      c = -1;
      while (++c < 4)
	{
	  argb[c] += cos * light->color.argb[c] * light->bright;
	  argb[c] *= pow((cos * (light->color.argb[c] / 0xFF) *
			  light->bright) + 1, 2);
	}
    }
}

t_color		set_light(t_scene scene, t_mlx_coord pt)
{
  t_light	*light;
  t_iobj	*between;
  t_color	color;
  unsigned int	argb[4];
  int		c;

  color = scene.ls_obj.near->color;
  c = -1;
  while (++c < 4)
    argb[c] = 0;
  light = scene.ls_light.first;
  while (light)
    {
      between = get_near_obj(scene.ls_obj,
      			     set_origin(pt, get_vect(light->pos, pt)),
			     scene.ls_obj.near);
      if (!between || between->dist > 1)
	update_light_color(scene.ls_obj.near, light, argb, pt);
      light = light->next;
    }
  c = -1;
  while (++c < 4)
    color.argb[c] *= (double)(argb[c] / (scene.ls_light.nb << 2)) / 0xFF;
  return (color);
}
