/*
** key_hook.c for rt in /home/charti_r/rendu/MUL_2013_rtracer
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Fri Apr  4 15:26:06 2014 charti
** Last update Sat Apr 12 16:57:49 2014 charti
*/

#include "rt.h"

int	key_hook(int keycode, t_param *param)
{
  if (keycode == MLX_KEY_ESC)
    escape(param);
  return (0);
}

void	escape(t_param *param)
{
  mlx_destroy_window(param->mlx_ptr, param->win_ptr);
  free(param->mlx_ptr);
  free(param->mlx_img->img_ptr);
  free(param->mlx_img);
  exit(0);
}
