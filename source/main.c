/*
** main.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Fri Apr  4 14:27:30 2014 charti
** Last update Sun Jun  8 14:18:11 2014 charti
*/

#include "rt.h"

#include "plane.h"
#include "sphere.h"
#include "cylinder.h"
#include "cone.h"

const t_tag	g_tag[] =
  {
    {PLANE, "plane", &set_plane},
    {SPHERE, "sphere", &set_sphere},
    {CYLINDER, "cylinder", &set_cylinder},
    {CONE, "cone", &set_cone},
    {DEFAULT, NULL, NULL}
  };

int	main(int argc, char **argv)
{
  char	*file;

  if (argc > 2)
    {
      my_fputstr(2, "bad usage: rt (file)\n");
      return (1);
    }
  if (argc == 2)
    {
      if ((file = load_file(argv[1])) == NULL)
	return (1);
    }
  else
    if ((file = load_file(DEFAULT_FILE)) == NULL)
      return (1);
  return (raytracer(file));
}
