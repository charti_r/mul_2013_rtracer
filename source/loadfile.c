/*
** loadfile.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Fri Apr  4 14:37:33 2014 charti
** Last update Sun Jun  8 14:20:03 2014 charti
*/

#include "rt.h"

/*
** Verify the extension of the file
*/
static int	check_file_format(char *filename)
{
  int		i;
  int		stop;

  if (!filename)
    return (1);
  stop = 0;
  i = -1;
  while (filename[++i] && !stop)
    if (!my_strcmp(&filename[i], ".xml"))
      stop = 1;
  if (!stop)
    return (1);
  return (0);
}

/*
** Return the FileDescriptor of the file, or -1 if it failed
*/
static int	open_file(char *filename)
{
  int		fd;

  if (check_file_format(filename))
    {
      my_fprintf(2, "%s: Wrong File Extension (*.xml)\n", filename);
      return (-1);
    }
  if ((fd = open(filename, O_RDONLY)) == -1)
    {
      my_fprintf(2, "%s: File defect\n", filename);
      return (-1);
    }
  return (fd);
}

/*
** Load the file in a string
*/
char	*load_file(char *filename)
{
  char	*file;
  char	buffer[512];
  int	fd;
  int	ret;
  int	i;

  if ((fd = open_file(filename)) == -1)
    return (NULL);
  file = NULL;
  i = 0;
  while ((ret = read(fd, buffer, 512)) > 0)
    {
      buffer[ret] = '\0';
      if ((file = my_str_realloc(file, (i * 512) + ret)) == NULL)
	return (NULL);
      if ((file = my_strcat(file, buffer)) == NULL)
	return (NULL);
      ++i;
    }
  close(fd);
  return (file);
}
