/*
** display.c for rt in /home/charti_r/rendu/MUL_2013_rtracer
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Apr  7 12:01:12 2014 charti
** Last update Fri Jun  6 10:43:54 2014 charti
*/

#include "rt.h"

/*
** Print the percentage bare on the std output
*/
static void	print_per_cent(float cent)
{
  int	i;

  my_putstr("\r[");
  i = 0;
  while (i < cent)
    {
      my_putchar('|');
      ++i;
    }
  while (i < NB_CENT)
    {
      my_putchar(' ');
      ++i;
    }
  my_printf("] %d%%", (int)(cent * (100 / NB_CENT)));
}

/*
** Calcul the image
*/
static void	get_img(t_param *param)
{
  t_mlx_coord	pt;
  t_mlx_coord	vect;
  t_color	color;

  pt.x = 0;
  while (pt.x < WD)
    {
      vect.y = -((WD * param->aliasing) >> 1) + (pt.x * param->aliasing);
      vect.y /= param->aliasing;
      pt.y = 0;
      while (pt.y < HT)
	{
	  vect.z = ((HT * param->aliasing) >> 1) - (pt.y * param->aliasing);
	  vect.z /= param->aliasing;
	  color = get_pixel_color(param, vect);
	  my_pixel_put_img(param->mlx_img, &pt, color.color);
	  ++pt.y;
	}
      ++pt.x;
      mlx_put_image_to_window(param->mlx_ptr, param->win_ptr,
			      param->mlx_img->img_ptr, 0, 0);
      print_per_cent((pt.x * HT * NB_CENT) / (WD * HT));
    }
  my_putchar('\n');
}

/*
** Display function clear, calculate and print a new image on the window
*/
int	display(t_param *param)
{
  my_clear_image(param->mlx_img);
  get_img(param);
  mlx_put_image_to_window(param->mlx_ptr, param->win_ptr,
			  param->mlx_img->img_ptr, 0, 0);
  return (0);
}
