/*
** direct_light.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Thu Apr 24 14:27:42 2014 charti
** Last update Sun Jun  8 17:34:46 2014 charti
*/

#include "rt.h"
#include "iobj.h"

static void	update_light(t_light *light, t_origin org, unsigned int argb[4])
{
  double	cos;
  int		c;

  cos = cos_vect(get_vect(light->pos, org.pos), org.rot);
  if (cos > 0.0)
    {
      c = -1;
      while (++c < 4)
	argb[c] += pow(cos, 550) * light->bright * light->color.argb[c];
    }
}

/*
** Calculate the Direct Light
*/
t_color		set_direct_light(t_scene scene, t_origin org,
				 t_color color)
{
  t_light	*light;
  unsigned int	argb[4];
  int		c;

  c = -1;
  while (++c < 4)
    argb[c] = 0;
  light = scene.ls_light.first;
  while (light)
    {
      if ((scene.ls_obj.near = get_near_obj(scene.ls_obj, org, NULL)) == NULL)
	update_light(light, org, argb);
      else if (scene.ls_obj.near->dist * get_vect_size(org.rot)
	       > get_vect_size(get_vect(light->pos, org.pos)))
	update_light(light, org, argb);
      light = light->next;
    }
  c = -1;
  while (++c < 4)
    color.argb[c] += ((argb[c] / scene.ls_light.nb)
		      * (0xFF - color.argb[c])) / 0xFF;
  return (color);
}
