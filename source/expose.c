/*
** expose.c for rt in /home/charti_r/rendu/MUL_2013_rtracer
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Fri Apr  4 15:28:50 2014 charti
** Last update Fri Apr  4 15:31:04 2014 charti
*/

#include "rt.h"

int	expose_hook(t_param *param)
{
  mlx_put_image_to_window(param->mlx_ptr, param->win_ptr,
			  param->mlx_img->img_ptr, 0, 0);
  return (0);
}
