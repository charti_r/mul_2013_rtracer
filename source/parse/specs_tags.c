/*
** specs_tag.c for specs_tag in /home/chauvi_r/Projets/mul_2013_rtracer
** 
** Made by chauvi_r
** Login   <chauvi_r@work>
** 
** Started on  Sat Apr 12 00:15:22 2014 chauvi_r
** Last update Sun Jun  8 18:34:50 2014 chauvi_r
*/

#include "rt.h"
#include "iobj.h"
#include "parse.h"
#include "cone.h"

int	get_bright(double *bright, char *file)
{
  int	i;
  char	buffer[25];
  int	j;

  j = 0;
  i = 0;
  while (file[i] && file[i] != '\n')
    {
      if ((file[i] >= '0' && file[i] <= '9') || file[i] == '.')
	buffer[j++] = file[i];
      ++i;
    }
  if (j != 0)
    *bright = atof(buffer) / 110;
  else
    return (1);
  if (*bright > 1)
    *bright = D_BRIGHT;
  return (0);
}

int	get_glint(t_iobj *parent, char *file)
{
  int	i;
  char	buffer[25];
  int	j;

  j = 0;
  i = 0;
  while (file[i] && file[i] != '\n')
    {
      if ((file[i] >= '0' && file[i] <= '9') || file[i] == '.')
	buffer[j++] = file[i];
      ++i;
    }
  if (j != 0)
    parent->glint = atof(buffer) / 100;
  else if (parent->glint < 0 || parent->glint > 1)
    parent->glint = D_GLINT;
  return (0);
}

int	get_height(char *file)
{
  int	i;
  int	height;

  i = 0;
  while (file[i] && file[i] != '\n' && (file[i] < '0' || file[i] > '9'))
    ++i;
  if (file[i] >= '0' && file[i] <= '9')
    height = my_getnbr(&file[i]);
  else
    height = D_HEIGHT;
  return (height);
}

int	get_radius(char *file)
{
  int	i;
  int	radius;

  i = 0;
  while (file[i] && file[i] != '\n' && (file[i] < '0' || file[i] > '9'))
    ++i;
  if (file[i] >= '0' && file[i] <= '9')
    radius = my_getnbr(&file[i]);
  else
    radius = D_RADIUS;
  return (radius);
}

void		get_slant(char *file, t_cone *cone)
{
  int		i;
  char		buffer[25];
  int		j;

  j = 0;
  i = 0;
  while (file[i] && file[i] != '\n')
    {
      if ((file[i] >= '0' && file[i] <= '9') || file[i] == '.')
	buffer[j++] = file[i];
      ++i;
    }
  if (j != 0)
    cone->slant = atof(buffer);
  else
    cone->slant = D_SLANT;
}
