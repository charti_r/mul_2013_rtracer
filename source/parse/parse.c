/*
** parse.c for parse in /home/chauvi_r/Projets/mul_2013_rtracer
** 
** Made by chauvi_r
** Login   <chauvi_r@work>
** 
** Started on  Tue Jun  3 00:08:45 2014 chauvi_r
** Last update Sun Jun  8 21:00:05 2014 chauvi_r
*/

#include "rt.h"
#include "iobj.h"
#include "parse.h"
#include "sphere.h"
#include "plane.h"
#include "cone.h"
#include "cylinder.h"

/*
** Check if the object is valid
*/
static int	compare_object(char *object)
{
  int		i;
  int		nb;

  i = 0;
  nb = 0;
  if (my_strcmp("eye", object) == 0 || my_strcmp("spot", object) == 0)
    return (0);
  while (g_tag[i].type != DEFAULT)
    {
      if (my_strcmp(g_tag[i].tag, object) == 0)
	nb = 1;
      ++i;
    }
  if (nb == 0)
    return (-1);
  else
    return (0);
}

/*
** Parsing of tag
*/
static int	parsing_xml(char *file, int i)
{
  while (file[i] && file[i] != '<' && file[i + 1] != '/')
    {
      if (file[i] == '\n' || file[i] == '\t' || file[i] == ' ')
	++i;
      if ((file[i] >= 'a' && file[i] <= 'z') || file[i] == '<' )
	return (i);
      ++i;
    }
  return (-1);
}

static int	get_tag_object(t_iobj *parent, t_param *param,
			       char *file, char *name)
{
  int		i;
  t_sphere	sphere;
  t_cylinder	cylinder;
  t_cone	cone;
  int		status[NB_OPTION];
  double	bright;

  i = 0;
  while ((i = parsing_xml(file, i)) != -1)
    {
      get_standard_tags(parent, status, &file[i], &bright);
      if (my_strcmp("sphere", name) == 0)
	status[5] = param_sphere(&file[i], &sphere, parent, &(param->scene));
      else if (my_strcmp("cylinder", name) == 0)
	status[5] = param_cylinder(&file[i], &cylinder, parent, &(param->scene));
      else if (my_strcmp("cone", name) == 0)
	status[5] = param_cone(&file[i], &cone, parent, &(param->scene));
      while (file[i] && file[i + 1] && file[i] != '\n' && file[i + 1] != '<')
	++i;
    }
  if (my_strcmp("plane", name) == 0)
    status[5] = param_plane(parent, &(param->scene));
  if (my_strcmp("eye", name) == 0 || my_strcmp("spot", name) == 0)
    status[5] = param_scene(bright, parent, &(param->scene), status);
  return (status[5]);
}

/*
** Check status of an object
** EXIST or NOT
** Call function to set and display the object
*/
static int	get_object(t_param *param, char *file)
{
  int		nb;
  char		*object;
  int		i;
  t_iobj	parent;

  nb = my_sizeof_char(file, '>');
  if ((object = malloc(sizeof(*object) * nb + 2)) == NULL)
    return (-1);
  i = 0;
  while (i < nb)
    {
      object[i] = file[i];
      ++i;
    }
  object[i] = '\0';
  while (file[i] && file[i] != '\n')
    ++i;
  nb = set_aliasing(&file[i], object, param);
  if (get_tag_object(&parent, param, &file[i], object) != -1
      && compare_object(object) != -1 && nb != 0)
    my_fprintf(1, "Object %s was set\n", object);
  else if (nb != 0)
    my_fprintf(2, "Impossible to set object %s\n", object);
  free(object);
  return (0);
}

int	get_tag(t_param *param, char *file)
{
  int	i;

  i = 0;
  while (file[i])
    {
      if (file[i + 1] && file[i] == '<' && file[i + 1] != '/')
	{
	  if ((get_object(param, &file[++i])) == -1)
	    return (-1);
	}
      ++i;
    }
  return (0);
}
