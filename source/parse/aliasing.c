/*
** aliasing.c for aliasing in /home/chauvi_r/mul_2013_rtracer
** 
** Made by chauvi_r
** Login   <chauvi_r@epitech.net>
** 
** Started on  Sun Jun  8 19:06:00 2014 chauvi_r
** Last update Sun Jun  8 20:01:32 2014 chauvi_r
*/

#include "rt.h"
#include "iobj.h"
#include "parse.h"

int	set_aliasing(char *file, char *name, t_param *param)
{
  int	i;

  if (my_strcmp("render", name) == 0)
    {
      i = 0;
      while (file[i] && file[i] != '<' && file[i] != 'a')
	++i;
      if (file[i] == 'a')
	{
	  if (my_strncmp("aliasing", &file[i], 7) == 0)
	    {
	      while (file[i] && file[i] != '\n'
		     && (file[i] < '0' || file[i] > '9'))
		++i;
	      if (file[i] >= '0' && file[i] <= '9')
		param->aliasing = my_getnbr(&file[i]);
	      if (param->aliasing < 1)
		param->aliasing = 1;
	      my_printf("Aliasing set : %d\n", param->aliasing);
	      my_printf("______________________\n");
	    }
	}
      return (0);
    }
  return (-1);
}
