/*
** object.c for object in /home/chauvi_r/Projets/mul_2013_rtracer
** 
** Made by chauvi_r
** Login   <chauvi_r@work>
** 
** Started on  Fri Jun  6 23:52:53 2014 chauvi_r
** Last update Sun Jun  8 20:07:00 2014 chauvi_r
*/

#include "rt.h"
#include "iobj.h"
#include "sphere.h"
#include "plane.h"
#include "cone.h"
#include "cylinder.h"
#include "parse.h"

/*
** Add specifities of the object
** if all parameters needed to display the object are present
** --> Add the object to the scene
*/
int		param_sphere(char *file, t_sphere *sphere,
		     t_iobj *parent, t_scene *scene)
{
  static int	gen = 0;

  if (gen == 0)
    {
      sphere->radius = D_RADIUS;
      ++gen;
    }
  if ((my_strcmp(file, P_RADIUS)) == ':')
    {
      sphere->radius = get_radius(file);
      if (add_obj(&scene->ls_obj, *parent, SPHERE, (t_iobj *)sphere))
	return (-1);
    }
  return (0);
}

/*
** Add specifities of the object
** if all parameters needed to display the object are present
** --> Add the object to the scene
*/
int		param_cylinder(char *file, t_cylinder *cylinder,
		     t_iobj *parent, t_scene *scene)
{
  static int	gen = 0;

  if (gen == 0)
    {
      cylinder->radius = D_RADIUS;
      cylinder->height = D_HEIGHT;
      ++gen;
    }
  if ((my_strcmp(file, P_RADIUS)) == ':')
    cylinder->radius = get_radius(file);
  else if ((my_strcmp(file, P_HEIGHT)) == ':')
    cylinder->height = get_height(file);
  if (file[0] == '<')
    {
      if (add_obj(&scene->ls_obj, *parent, CYLINDER, (t_iobj *)cylinder))
	return (-1);
    }
  return (0);
}

int		param_cone(char *file, t_cone *cone,
		     t_iobj *parent, t_scene *scene)
{
  static int	gen = 0;

  if (gen == 0)
    {
      cone->slant = D_SLANT;
      cone->height = D_HEIGHT;
      ++gen;
    }
  if ((my_strcmp(file, P_HEIGHT)) == ':')
    cone->height = get_height(file);
  else if ((my_strcmp(file, P_SLANT)) == ':')
    get_slant(file, cone);
  if (file[0] == '<')
    {
      if (add_obj(&scene->ls_obj, *parent, CONE, (t_iobj *)cone))
	return (-1);
    }
  return (0);
}

/*
** if all parameters needed to add light or eye
** --> Add the eye / light to the scene
*/
int		param_scene(double bright, t_iobj *parent,
		    t_scene *scene, int status[])
{
  static int	gen = 0;

  if (status[0] == 0 && status[1] == 0 && bright < 0 && gen == 0)
    {
      scene->eye = set_origin(parent->pos, parent->rot);
      ++gen;
      return (0);
    }
  else if (status[0] == 0 && status[2] == 0 && status[4] == 0
	   && bright <= 1)
    {
      if (add_light(&scene->ls_light, parent->pos,
		    parent->color.color, bright))
	return (-1);
    }
  return (0);
}

/*
** Add specifities of the object
** if all parameters needed to display the object are present
** --> Add the object to the scene
*/
int		param_plane(t_iobj *parent, t_scene *scene)
{
  t_plane	plane;

  if (add_obj(&scene->ls_obj, *parent, PLANE, (t_iobj *)&plane))
    return (-1);
  return (0);
}
