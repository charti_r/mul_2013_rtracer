/*
** tag.c for tag in /home/chauvi_r/Projets/mul_2013_rtracer
** 
** Made by chauvi_r
** Login   <chauvi_r@work>
** 
** Started on  Sat Apr 12 00:15:22 2014 chauvi_r
** Last update Sun Jun  8 17:42:27 2014 chauvi_r
*/

#include "rt.h"
#include "iobj.h"
#include "parse.h"

int	coord_from_file(t_iobj *parent, char *file, int mode)
{
  int	i;
  int	pos[3];
  int	nb;

  nb = 0;
  i = 0;
  while (file[i] && file[i] != '\n')
    {
      if ((file[i] >= '0' && file[i] <= '9' && nb != 3) || (file[i] == '-'))
	{
	  pos[nb++] = my_getnbr(&file[i]);
	  while (file[i] && file[i] != '\n' && file[i] != ',')
	    ++i;
	}
      else
	++i;
    }
  while (nb < 2)
    pos[nb++] = 0;
  if (mode == 1)
    parent->pos = set_coord(pos[0], pos[1], pos[2]);
  else
    parent->rot = set_coord(pos[0], pos[1], pos[2]);
  return (0);
}

int	color_from_file(t_iobj *parent, char *file)
{
  int	i;

  i = 0;
  while (file[i] && file[i] != '\n')
    {
      if (file[i] == '0' && file[i + 1] == 'x')
	{
	  i += 2;
	  parent->color.color = my_getnbr_base(&file[i], "0123456789ABCDEF");
	  return (0);
	}
      ++i;
    }
  parent->color.color = MLX_WHITE;
  return (0);
}

void	get_standard_tags(t_iobj *parent, int *status,
				  char *file, double *bright)
{
  static int	gen = 0;

  if (gen == 0)
    {
      *bright = -1;
      ++gen;
    }
  if (my_strcmp(file, P_POS) == ':')
    status[0] = coord_from_file(parent, file, 1);
  else if (my_strcmp(file, P_ROT) == ':')
    status[1] = coord_from_file(parent, file, 2);
  else if (my_strcmp(file, P_COLOR) == ':')
    status[2] = color_from_file(parent, file);
  else if (my_strcmp(file, P_GLINT) == ':')
    status[3] = get_glint(parent, file);
  else if (my_strcmp(file, P_BRIGHT) == ':')
    status[4] = get_bright(bright, file);
}
