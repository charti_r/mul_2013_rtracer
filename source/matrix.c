/*
** matrix.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Tue Apr  8 17:03:36 2014 charti
** Last update Tue May 27 08:32:27 2014 charti
*/

#include "rt.h"

/*
** Apply a rotation(rot) on the Vector(vect)
*/
void    vect_rot(t_mlx_coord *vect, t_mlx_coord rot)
{
  matrix_rot_x(vect, rot.x);
  matrix_rot_y(vect, rot.y);
  matrix_rot_z(vect, rot.z);
}

/*
** Apply a reverse rotation(rot) on the Vector(vect)
*/
void    vect_invrot(t_mlx_coord *vect, t_mlx_coord rot)
{
  matrix_rot_x(vect, -(rot.x));
  matrix_rot_y(vect, -(rot.y));
  matrix_rot_z(vect, -(rot.z));
}

/*
** Apply an x axe rotation
** to apply an reverse rotation, multipli the rotation by -1
*/
void		matrix_rot_x(t_mlx_coord *vect, int rot)
{
  t_mlx_coord	new_m;
  double	cos_rot;
  double	sin_rot;

  cos_rot = cos(RAD(rot));
  sin_rot = sin(RAD(rot));
  new_m.y = (vect->y * cos_rot) - (vect->z * sin_rot);
  new_m.z = (vect->y * sin_rot) + (vect->z * cos_rot);
  vect->y = new_m.y;
  vect->z = new_m.z;
}

/*
** Apply an y axe rotation
** to apply an reverse rotation, multipli the rotation by -1
*/
void		matrix_rot_y(t_mlx_coord *vect, int rot)
{
  t_mlx_coord	new_m;
  double	cos_rot;
  double	sin_rot;

  cos_rot = cos(RAD(rot));
  sin_rot = sin(RAD(rot));
  new_m.x = (vect->x * cos_rot) + (vect->z * sin_rot);
  new_m.z = (vect->z * cos_rot) - (vect->x * sin_rot);
  vect->x = new_m.x;
  vect->z = new_m.z;
}

/*
** Apply an z axe rotation
** to apply an reverse rotation, multipli the rotation by -1
*/
void		matrix_rot_z(t_mlx_coord *vect, int rot)
{
  t_mlx_coord	new_m;
  double	cos_rot;
  double	sin_rot;

  cos_rot = cos(RAD(rot));
  sin_rot = sin(RAD(rot));
  new_m.x = (vect->x * cos_rot) - (vect->y * sin_rot);
  new_m.y = (vect->x * sin_rot) + (vect->y * cos_rot);
  vect->x = new_m.x;
  vect->y = new_m.y;
}
