/*
** math.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Tue Apr  8 17:02:10 2014 charti
** Last update Thu Jun  5 15:00:27 2014 charti
*/

#include "rt.h"

/*
** return a t_mlx_coord directly set with x, y and z
*/
t_mlx_coord	set_coord(double x, double y, double z)
{
  t_mlx_coord	pt;

  pt.x = x;
  pt.y = y;
  pt.z = z;
  return (pt);
}

/*
** return a t_origin set with its position and its rotation / view angle
*/
t_origin	set_origin(t_mlx_coord pos, t_mlx_coord rot)
{
  t_origin	org;

  org.pos = pos;
  org.rot = rot;
  return (org);
}

/*
** Calculate a coordinate return in a t_mlx_coord
** with an origin point and a distance
*/
t_mlx_coord	get_pt(double dist, t_origin origin)
{
  t_mlx_coord	pt;

  pt.x = origin.pos.x + (dist * origin.rot.x);
  pt.y = origin.pos.y + (dist * origin.rot.y);
  pt.z = origin.pos.z + (dist * origin.rot.z);
  return (pt);
}
