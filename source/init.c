/*
** init.c for rt in /home/charti_r/rendu/MUL_2013_rtracer
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Fri Apr  4 15:06:04 2014 charti
** Last update Sun Jun  8 21:02:01 2014 chauvi_r
*/

#include "rt.h"
#include "iobj.h"
#include "plane.h"
#include "sphere.h"
#include "cylinder.h"
#include "cone.h"
#include "tore.h"
#include "parse.h"

/*
** To add an object
*/
int	add_obj(t_ls_obj *ls_obj, t_iobj parent,
			t_type type, t_iobj *obj)
{
  int		i;

  i = 0;
  while (g_tag[i].type != type && g_tag[i].type != DEFAULT)
    ++i;
  if (g_tag[i].type == DEFAULT)
    return (1);
  if ((ls_obj->first = g_tag[i].set_obj(ls_obj->first, parent, obj)) == NULL)
    return (1);
  return (0);
}

/*
** To add a light
*/
int	add_light(t_ls_light *ls_light, t_mlx_coord pos,
			  int color, double bright)
{
  t_light	*light;

  if ((light = malloc(sizeof(*light))) == NULL)
    return (1);
  light->pos = pos;
  light->color.color = color;
  light->bright = bright;
  light->next = ls_light->first;
  ls_light->first = light;
  ++(ls_light->nb);
  return (0);
}

/*
** Init the scene param
*/
static void	init_scene(t_scene *scene)
{
  scene->ls_obj.near = NULL;
  scene->ls_obj.first = NULL;
  scene->ls_light.nb = 0;
  scene->ls_light.first = NULL;
}

/*
** Init Param
*/
int	init_param(t_param *param, char *file)
{
  if ((param->mlx_ptr = mlx_init()) == NULL)
    {
      my_fputstr(2, "Error : the environment is not set correctly.\n");
      return (1);
    }
  param->win_ptr = mlx_new_window(param->mlx_ptr, WD, HT, WIN_NAME);
  if (param->win_ptr == NULL)
    return (1);
  if ((param->mlx_img = my_mlx_init_img(param->mlx_ptr, WD, HT)) == NULL)
    return (1);
  init_scene(&(param->scene));
  param->aliasing = 1;
  if (get_tag(param, file) == -1)
    return (1);
  return (0);
}
