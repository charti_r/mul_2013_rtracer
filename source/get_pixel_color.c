/*
** get_pixel_color.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Tue Apr  8 08:12:50 2014 charti
** Last update Fri Jun  6 10:21:34 2014 charti
*/

#include "rt.h"
#include "iobj.h"

/*
** Apply the eye rot to the final angle view
*/
static t_origin	rot_eye(t_origin origin, t_mlx_coord vect)
{
  vect_rot(&vect, origin.rot);
  origin.rot = vect;
  return (origin);
}

/*
** Place correctly an object at the origin to calculate its distance
*/
static t_origin	simple_pos(t_origin origin, t_iobj *obj)
{
  origin.pos = get_vect(origin.pos, obj->pos);
  vect_invrot(&origin.pos, obj->rot);
  vect_invrot(&origin.rot, obj->rot);
  return (origin);
}

static t_color	anti_aliasing(t_scene scene, t_mlx_coord vect)
{
  t_color	color;
  t_origin	org;
  t_mlx_coord	pt;

  color.color = 0x00;
  org = rot_eye(scene.eye, vect);
  if ((scene.ls_obj.near = get_near_obj(scene.ls_obj, org, NULL)) != NULL)
    {
      color = scene.ls_obj.near->color;
      pt = get_pt(scene.ls_obj.near->dist, org);
      color = set_light(scene, pt);
      if (scene.ls_obj.near->glint > 0.0)
	color = set_glint(scene, org, color, pt);
    }
  color = set_direct_light(scene, org, color);
  return (color);
}

/*
** Return the pixel color needed
*/
t_color		get_pixel_color(t_param *param, t_mlx_coord vect)
{
  t_color	color;
  t_mlx_coord	pt;
  int		argb[4];
  int		i;

  i = -1;
  while (++i < 4)
    argb[i] = 0;
  pt.x = DISTANCE;
  pt.y = vect.y - 1;
  while (++pt.y < (vect.y + param->aliasing))
    {
      pt.z = vect.z - 1;
      while (++pt.z < (vect.z + param->aliasing))
	{
	  color = anti_aliasing(param->scene, pt);
	  i = -1;
	  while (++i < 4)
	    argb[i] += color.argb[i];
	}
    }
  i = -1;
  while (++i < 4)
    color.argb[i] = (argb[i] / POW2(param->aliasing));
  return (color);
}

/*
** Return the nearest object from an origin
*/
t_iobj		*get_near_obj(t_ls_obj ls_obj, t_origin origin,
			      t_iobj *no)
{
  double	tmp;
  t_iobj	*near;
  t_iobj	*obj;

  tmp = 0.0;
  near = NULL;
  obj = ls_obj.first;
  while (obj)
    {
      obj->dist = obj->get_dist(obj, simple_pos(origin, obj));
      if (obj != no && obj->dist > 0.0 && (!near || obj->dist < tmp))
	{
	  tmp = obj->dist;
	  near = obj;
	}
      obj = obj->next;
    }
  return (near);
}
