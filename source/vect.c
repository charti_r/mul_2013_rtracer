/*
** vect.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/header
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Fri Apr 11 10:03:31 2014 charti
** Last update Thu Jun  5 14:03:34 2014 charti
*/

#include "rt.h"

/*
** Calculate a Vector between two points (p1 - p2)
*/
t_mlx_coord	get_vect(t_mlx_coord p1, t_mlx_coord p2)
{
  return (set_coord(p1.x - p2.x, p1.y - p2.y, p1.z - p2.z));
}

/*
** Return the Vector size
*/
double		get_vect_size(t_mlx_coord vect)
{
  return (sqrt(POW2(vect.x) + POW2(vect.y) + POW2(vect.z)));
}

/*
** return the cosinus between two Vectors
*/
double		cos_vect(t_mlx_coord v1, t_mlx_coord v2)
{
  return (get_dot_product(v1, v2) / (get_vect_size(v1) * get_vect_size(v2)));
}

/*
** Calculate the Dot Product between two Vectors
*/
double		get_dot_product(t_mlx_coord vect1, t_mlx_coord vect2)
{
  double	dot_prod;

  dot_prod = vect1.x * vect2.x;
  dot_prod += vect1.y * vect2.y;
  dot_prod += vect1.z * vect2.z;
  return (dot_prod);
}

/*
** Reverse the Vector direction
*/
t_mlx_coord	vect_invdir(t_mlx_coord vect)
{
  vect.x *= -1;
  vect.y *= -1;
  vect.z *= -1;
  return (vect);
}
