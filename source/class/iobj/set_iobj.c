/*
** set_iobj.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source/class/iobj
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Tue Apr  8 02:36:59 2014 charti
** Last update Sat Apr 12 17:17:48 2014 charti
*/

#include "iobj.h"

/*
** Parent constructor
*/
void	set_iobj(t_iobj *obj, t_iobj parent, t_type type, t_iobj *first)
{
  obj->pos = parent.pos;
  obj->rot = parent.rot;
  obj->color = parent.color;
  obj->glint = parent.glint;
  obj->dist = 0.0;
  obj->type = type;
  obj->next = first;
}
