/*
** set_cone.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source/class/cone
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Apr  9 10:48:40 2014 charti
** Last update Sun Jun  8 17:54:15 2014 charti
*/

#include "cone.h"

t_iobj		*set_cone(t_iobj *first, t_iobj parent, t_iobj *obj)
{
  t_cone	*cone;

  if ((cone = malloc(sizeof(*cone))) == NULL)
    return (NULL);
  set_iobj(&cone->parent, parent, CONE, first);
  cone->parent.get_dist = get_dist_cone;
  cone->parent.get_normal = get_normal_cone;
  cone->radius = ((t_cone*)(obj))->radius;
  cone->height = ((t_cone*)(obj))->height;
  cone->slant = ((t_cone*)(obj))->slant;
  return ((t_iobj*)cone);
}
