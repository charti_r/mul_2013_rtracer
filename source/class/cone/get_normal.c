/*
** get_normal.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source/class/cone
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Apr  9 12:39:14 2014 charti
** Last update Sun Jun  8 17:54:46 2014 charti
*/

#include "cone.h"

t_mlx_coord	get_normal_cone(t_iobj *obj, t_mlx_coord pt)
{
  t_cone	*cone;

  cone = (t_cone*)obj;
  return (set_coord(pt.x - obj->pos.x, pt.y - obj->pos.y,
		    (pt.z - obj->pos.z) * (-cone->slant)));
}
