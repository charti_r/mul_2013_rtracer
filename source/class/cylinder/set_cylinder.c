/*
** set_cylinder.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source/class/cylinder
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Apr  9 10:49:05 2014 charti
** Last update Sun Jun  8 17:53:48 2014 charti
*/

#include "cylinder.h"

t_iobj		*set_cylinder(t_iobj *first, t_iobj parent, t_iobj *obj)
{
  t_cylinder	*cylinder;

  if ((cylinder = malloc(sizeof(*cylinder))) == NULL)
    return (NULL);
  set_iobj(&cylinder->parent, parent, CYLINDER, first);
  cylinder->parent.get_dist = get_dist_cylinder;
  cylinder->parent.get_normal = get_normal_cylinder;
  cylinder->radius = ((t_cylinder*)(obj))->radius;
  cylinder->height = ((t_cylinder*)(obj))->height;
  return ((t_iobj*)cylinder);
}
