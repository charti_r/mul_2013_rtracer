/*
** get_dist.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source/class/cylinder
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Apr  9 10:49:16 2014 charti
** Last update Sun Jun  8 17:54:02 2014 charti
*/

#include "cylinder.h"

static double	edge(double dist[], double height, t_origin org);

double	get_dist_cylinder(t_iobj *obj, t_origin origin)
{
  t_cylinder	*cylinder;
  double	dist[2];
  double	delta;
  t_mlx_coord	pt;

  cylinder = (t_cylinder*)obj;
  pt.x = POW2(origin.rot.x) + POW2(origin.rot.y);
  pt.y = 2 * ((origin.pos.x * origin.rot.x) + (origin.pos.y * origin.rot.y));
  pt.z = (POW2(origin.pos.x) + POW2(origin.pos.y) - POW2(cylinder->radius));
  delta = POW2(pt.y) - (4 * pt.x * pt.z);
  if (delta < 0.0)
    return (0.0);
  delta = sqrt(delta) / (pt.x * 2);
  dist[0] = -(pt.y / (pt.x * 2));
  dist[1] = dist[0] - delta;
  dist[0] = dist[0] + delta;
  return (edge(dist, cylinder->height, origin));
}

static double	edge(double dist[], double height, t_origin org)
{
  t_mlx_coord	pt[2];

  pt[0] = get_pt(dist[0], org);
  pt[1] = get_pt(dist[1], org);
  height /= 2;
  if (dist[0] > 0.0 && pt[0].z < height && pt[0].z > -height)
    if (dist[1] > 0.0 && pt[1].z < height && pt[1].z > -height)
      return (dist[0] > dist[1] ? dist[1] : dist[0]);
  if (dist[1] > 0.0 && pt[1].z < height && pt[1].z > -height)
    return (dist[1]);
  return (0.0);
}
