/*
** get_normal.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source/class/cylinder
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Apr  9 12:43:38 2014 charti
** Last update Thu Apr 10 15:06:10 2014 charti
*/

#include "cylinder.h"

t_mlx_coord	get_normal_cylinder(t_iobj *obj, t_mlx_coord pt)
{
  return (set_coord(pt.x - obj->pos.x, pt.y - obj->pos.y, 0));
}
