/*
** set_plane.c for rt in /home/charti_r/rendu/MUL_2013_rtracer
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Tue Apr  8 01:24:23 2014 charti
** Last update Sun Jun  8 17:42:44 2014 charti
*/

#include "plane.h"

t_iobj		*set_plane(t_iobj *first, t_iobj parent, t_iobj *obj)
{
  t_plane	*plane;

  (void)(obj);
  if ((plane = malloc(sizeof(*plane))) == NULL)
    return (NULL);
  set_iobj(&plane->parent, parent, PLANE, first);
  plane->parent.get_dist = get_dist_plane;
  plane->parent.get_normal = get_normal_plane;
  return ((t_iobj*)plane);
}
