/*
** get_normal.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source/class/plane
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Apr  9 12:50:48 2014 charti
** Last update Sun Jun  8 17:44:05 2014 charti
*/

#include "plane.h"

t_mlx_coord	get_normal_plane(t_iobj *obj, t_mlx_coord pt)
{
  (void)(obj);
  (void)(pt);
  return (set_coord(0, 0, DISTANCE));
}
