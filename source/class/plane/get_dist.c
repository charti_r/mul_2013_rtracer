/*
** get_dist.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source/class
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Apr  9 10:51:08 2014 charti
** Last update Sun Jun  8 17:42:22 2014 charti
*/

#include "plane.h"

double	get_dist_plane(t_iobj *obj, t_origin origin)
{
  (void)(obj);
  if (origin.rot.z)
    return (-origin.pos.z / origin.rot.z);
  return (0.0);
}
