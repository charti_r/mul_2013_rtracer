/*
** set_sphere.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source/class/sphere
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Apr  9 10:49:36 2014 charti
** Last update Sun Jun  8 17:52:50 2014 charti
*/

#include "sphere.h"

t_iobj		*set_sphere(t_iobj *first, t_iobj parent, t_iobj *obj)
{
  t_sphere	*sphere;

  if ((sphere = malloc(sizeof(*sphere))) == NULL)
    return (NULL);
  set_iobj(&sphere->parent, parent, SPHERE, first);
  sphere->parent.get_dist = get_dist_sphere;
  sphere->parent.get_normal = get_normal_sphere;
  sphere->radius = ((t_sphere*)(obj))->radius;
  return ((t_iobj*)sphere);
}
