/*
** get_dist.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source/class/sphere
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Apr  9 10:49:47 2014 charti
** Last update Sun Jun  8 17:53:30 2014 charti
*/

#include "sphere.h"

double		get_dist_sphere(t_iobj *obj, t_origin origin)
{
  t_sphere	*sphere;
  double	dist[2];
  double	delta;
  t_mlx_coord	pt;

  sphere = (t_sphere*)obj;
  pt.x = POW2(origin.rot.x) + POW2(origin.rot.y) + POW2(origin.rot.z);
  pt.y = ((origin.pos.x * origin.rot.x)
	  + (origin.pos.y * origin.rot.y)
	  + (origin.pos.z * origin.rot.z)) * 2;
  pt.z = (POW2(origin.pos.x) + POW2(origin.pos.y) + POW2(origin.pos.z) -
	  POW2(sphere->radius));
  if ((delta = POW2(pt.y) - (4 * pt.x * pt.z)) < 0.0)
    return (0.0);
  delta = sqrt(delta) / (pt.x * 2);
  dist[0] = -(pt.y / (pt.x * 2));
  dist[1] = dist[0] - delta;
  dist[0] = dist[0] + delta;
  if (dist[0] > 0.0)
    if (dist[1] > 0.0)
      return (dist[0] > dist[1] ? dist[1] : dist[0]);
  if (dist[1] > 0.0)
    return (dist[1]);
  return (0.0);
}
