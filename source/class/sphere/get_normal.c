/*
** get_normal.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source/class/sphere
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Apr  9 12:52:43 2014 charti
** Last update Thu Apr 10 15:43:36 2014 charti
*/

#include "sphere.h"

t_mlx_coord	get_normal_sphere(t_iobj *obj, t_mlx_coord pt)
{
  return (set_coord(pt.x - obj->pos.x, pt.y - obj->pos.y, pt.z - obj->pos.z));
}
