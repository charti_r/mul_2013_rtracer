/*
** raytracer.c for rt in /home/charti_r/rendu/MUL_2013_rtracer/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Fri Apr  4 14:36:28 2014 charti
** Last update Fri Jun  6 11:40:52 2014 charti
*/

#include "rt.h"

/*
** Run the Display and init hooks
*/
int		raytracer(char *file)
{
  t_param	param;

  if (init_param(&param, file))
    return (1);
  if (display(&param))
    return (1);
  mlx_key_hook(param.win_ptr, key_hook, &param);
  mlx_expose_hook(param.win_ptr, expose_hook, &param);
  mlx_loop(param.mlx_ptr);
  return (0);
}
