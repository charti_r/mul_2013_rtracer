/*
** structure.h for rt in /home/charti_r/rendu/MUL_2013_rtracer
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Fri Apr  4 15:01:18 2014 charti
** Last update Thu Jun  5 10:46:06 2014 charti
*/

#ifndef STRUCTURE_H_
# define STRUCTURE_H_

struct s_iobj;

/*
** Define the tag type
*/
typedef struct	s_tag
{
  t_type	type;
  char		*tag;
  struct s_iobj	*(*set_obj)(struct s_iobj *first,
			    struct s_iobj parent,
			    struct s_iobj *obj);
}		t_tag;

/*
** Origin
*/
typedef struct	s_origin
{
  t_mlx_coord	pos;
  t_mlx_coord	rot;
}		t_origin;

/*
** Light Elem
*/
typedef struct		s_light
{
  t_mlx_coord		pos;
  t_color		color;
  double		bright;
  struct s_light	*next;
}			t_light;

/*
** Light List
*/
typedef struct		s_ls_light
{
  int			nb;
  struct s_light	*first;
}			t_ls_light;

/*
** Obj List
*/
typedef struct		s_ls_obj
{
  struct s_iobj		*near;
  struct s_iobj		*first;
}			t_ls_obj;

/*
** Scene
*/
typedef struct	s_scene
{
  t_origin	eye;
  t_ls_obj	ls_obj;
  t_ls_light	ls_light;
}		t_scene;

/*
** Parameter
*/
typedef struct	s_param
{
  void		*mlx_ptr;
  void		*win_ptr;
  t_mlx_img	*mlx_img;
  t_scene	scene;
  int		aliasing;
}		t_param;

#endif /* !STRUCTURE_H_ */
