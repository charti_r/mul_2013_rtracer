/*
** enum.h for rt in /home/charti_r/rendu/MUL_2013_rtracer
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Tue Apr  8 01:02:21 2014 charti
** Last update Mon Jun  2 13:21:12 2014 charti
*/

#ifndef ENUM_H_
# define ENUM_H_

typedef enum	e_type
{
  PLANE = 0,
  SPHERE,
  CYLINDER,
  CONE,
  TORE,
  VOID_CUBE,
  DEFAULT
}		t_type;

#endif /* !ENUM_H_ */
