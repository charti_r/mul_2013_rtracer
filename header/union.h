/*
** union.h for rt in /home/charti_r/rendu/MUL_2013_rtracer/header
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Thu Apr 10 12:13:46 2014 charti
** Last update Mon Apr 14 14:48:17 2014 charti
*/

#ifndef UNION_H_
# define UNION_H_

/*
** Color
*/
typedef union	u_color
{
  unsigned int	color;
  unsigned char	argb[4];
}		t_color;

#endif /* !UNION_H_ */
