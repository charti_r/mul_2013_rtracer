/*
** parse.h for parse in /home/chauvi_r/Documents/mul_2013_rtracer
** 
** Made by chauvi_r
** Login   <chauvi_r@work>
** 
** Started on  Thu Apr 10 22:47:00 2014 chauvi_r
** Last update Sun Jun  8 20:05:19 2014 chauvi_r
*/

#ifndef PARSE_H_
# define PARSE_H_

# define NB_OPTION 5

/*
** SYNTAX PARSING
*/
# define P_POS		"pos"
# define P_ROT		"rot"
# define P_COLOR	"color"
# define P_RADIUS	"radius"
# define P_HEIGHT	"height"
# define P_GLINT	"glint"
# define P_SLANT	"slant"
# define P_BRIGHT	"bright"

/*
** DEFAULT VALUE IF VALUE WAS NOT SET CORRECTLY
*/
# define D_RADIUS	200
# define D_HEIGHT	200
# define D_BRIGHT	0.5
# define D_GLINT	0.1
# define D_SLANT	0.1

int	add_light(t_ls_light *ls_light, t_mlx_coord pos,
		  int color, double bright);
int	get_tag(t_param *param, char *file);
int	coord_from_file(t_iobj *parent, char *file, int mode);
int	color_from_file(t_iobj *parent, char *file);
int	get_bright(double *bright, char *file);
int	get_glint(t_iobj *parent, char *file);
int	get_height(char *file);
int	get_radius(char *file);
void	get_standard_tags(t_iobj *parent, int *status,
			  char *file, double *bright);
int	set_aliasing(char *file, char *name, t_param *param);
int	add_obj(t_ls_obj *ls_obj, t_iobj parent,
		t_type type, t_iobj *obj);

#endif /* !PARSE_H_ */
