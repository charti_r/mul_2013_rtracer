/*
** sphere.h for sphere in /home/charti_r/rendu/MUL_2013_rtracer/header/class
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Tue Apr  8 04:48:08 2014 charti
** Last update Fri Apr 11 15:54:04 2014 charti
*/

#ifndef SPHERE_H_
# define SPHERE_H_

# include "iobj.h"

typedef struct	s_sphere
{
  t_iobj	parent;
  double	radius;
}		t_sphere;

t_iobj		*set_sphere(t_iobj *first, t_iobj parent, t_iobj *obj);
double		get_dist_sphere(t_iobj *obj, t_origin origin);
t_mlx_coord	get_normal_sphere(t_iobj *obj, t_mlx_coord pt);
int		param_sphere(char *file, t_sphere *sphere,
			     t_iobj *parent, t_scene *scene);
#endif /* !SPHERE_H_ */
