/*
** cylinder.h for rt in /home/charti_r/rendu/MUL_2013_rtracer
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Tue Apr  8 02:22:38 2014 charti
** Last update Fri Apr 11 15:54:17 2014 charti
*/

#ifndef CYLINDER_H_
# define CYLINDER_H_

# include "iobj.h"

typedef struct	s_cylinder
{
  t_iobj	parent;
  double	radius;
  double	height;
}		t_cylinder;

t_iobj		*set_cylinder(t_iobj *first, t_iobj parent, t_iobj *obj);
double		get_dist_cylinder(t_iobj *obj, t_origin origin);
t_mlx_coord	get_normal_cylinder(t_iobj *obj, t_mlx_coord pt);
int		param_cylinder(char *file, t_cylinder *cylinder,
			       t_iobj *parent, t_scene *scene);

#endif /* !CYLINDER_H_ */
