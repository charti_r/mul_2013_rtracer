/*
** void_cube.h for rt in /home/charti_r/rendu/MUL_2013_rtracer/header/class
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Tue Apr 15 15:19:35 2014 charti
** Last update Tue Apr 15 16:19:14 2014 charti
*/

#ifndef VOID_CUBE_H_
# define VOID_CUBE_H_

# include "iobj.h"

typedef struct	s_void_cube
{
  t_iobj	parent;
  double	gap;
  double	size;
}		t_void_cube;

t_iobj		*set_void_cube(t_iobj *first, t_iobj parent, t_iobj *obj);
double		get_dist_void_cube(t_iobj *obj, t_origin origin);
t_mlx_coord	get_normal_void_cube(t_iobj *obj, t_mlx_coord pt);

#endif /* !VOID_CUBE_H_ */
