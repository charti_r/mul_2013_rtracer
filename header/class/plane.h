/*
** plane.h for rt in /home/charti_r/rendu/MUL_2013_rtracer
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Tue Apr  8 02:13:08 2014 charti
** Last update Sun Jun  8 16:06:36 2014 chauvi_r
*/

#ifndef PLANE_H_
# define PLANE_H_

# include "iobj.h"

typedef struct	s_plane
{
  t_iobj	parent;
}		t_plane;

t_iobj		*set_plane(t_iobj *first, t_iobj parent, t_iobj *obj);
double		get_dist_plane(t_iobj *obj, t_origin origin);
t_mlx_coord	get_normal_plane(t_iobj *obj, t_mlx_coord pt);
int		param_plane(t_iobj *parent, t_scene *scene);

#endif /* !PLANE_H_ */
