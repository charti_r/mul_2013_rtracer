/*
** iobj.h for rt in /home/charti_r/rendu/MUL_2013_rtracer
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Tue Apr  8 02:16:29 2014 charti
** Last update Sun Jun  8 16:05:30 2014 chauvi_r
*/

#ifndef IOBJ_H_
# define IOBJ_H_

# include "rt.h"

typedef struct	s_iobj
{
  t_type	type;
  t_mlx_coord	pos;
  t_mlx_coord	rot;
  t_color	color;
  double	glint;
  double	dist;
  double	(*get_dist)(struct s_iobj *obj, t_origin origin);
  t_mlx_coord	(*get_normal)(struct s_iobj *obj, t_mlx_coord pt);
  struct s_iobj	*next;
}		t_iobj;

void	set_iobj(t_iobj *obj, t_iobj parent, t_type type, t_iobj *first);
int	param_scene(double bright, t_iobj *parent,
		    t_scene *scene, int status[]);

#endif /* !IOBJ_H_ */
