/*
** tore.h for rt in /home/charti_r/rendu/MUL_2013_rtracer/header/class/
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Thu Apr 17 12:57:29 2014 charti
** Last update Thu Apr 17 13:06:22 2014 charti
*/

#ifndef TORE_H_
# define TORE_H_

# include "iobj.h"

typedef struct	s_tore
{
  t_iobj	parent;
  double	b_radius;
  double	l_radius;
}		t_tore;

t_iobj		*set_tore(t_iobj *first, t_iobj parent, t_iobj *obj);
double		get_dist_tore(t_iobj *obj, t_origin origin);
t_mlx_coord	get_normal_tore(t_iobj *obj, t_mlx_coord pt);

#endif /* !TORE_H_ */
