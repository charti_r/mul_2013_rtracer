/*
** cone.h for rt in /home/charti_r/rendu/MUL_2013_rtracer
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Tue Apr  8 02:25:22 2014 charti
** Last update Fri Apr 11 15:53:01 2014 charti
*/

#ifndef CONE_H_
# define CONE_H_

# include "iobj.h"

typedef struct	s_cone
{
  t_iobj	parent;
  double	radius;
  double	height;
  double	slant;
}		t_cone;

t_iobj		*set_cone(t_iobj *first, t_iobj parent, t_iobj *obj);
double		get_dist_cone(t_iobj *obj, t_origin origin);
t_mlx_coord	get_normal_cone(t_iobj *obj, t_mlx_coord pt);
void		get_slant(char *file, t_cone *cone);
int		param_cone(char *file, t_cone *cone,
			   t_iobj *parent, t_scene *scene);

#endif /* !CONE_H_ */
