/*
** rt.h for rt in /home/charti_r/rendu/MUL_2013_rtracer/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Fri Apr  4 14:31:16 2014 charti
** Last update Sun Jun  8 17:30:40 2014 charti
*/

#ifndef RT_H_
# define RT_H_

/*
** Std Inlcude
*/
# include <mlx.h>
# include <stdlib.h>
# include <math.h>

/*
** My Lib
*/
# include "my.h"
# include "my_mlx.h"

/*
** Definition of Elems
*/
# include "constante.h"
# include "enum.h"
# include "union.h"
# include "structure.h"

/*****************************************************************************/
/*****************************************************************************/

/*
** Loading File
*/
char	*load_file(char *filename);

/*
** Init
*/
int	init_param(t_param *param, char *file);

/*
** Raytracer
*/
int	raytracer(char *file);

/*
** Display
*/
int	display(t_param *param);

/*
** Calculs...
*/
t_color		get_pixel_color(t_param *param, t_mlx_coord vect);
struct s_iobj	*get_near_obj(t_ls_obj ls_obj, t_origin origin,
			      struct s_iobj *no);

/*
** Light
*/
t_color	set_light(t_scene scene, t_mlx_coord pt);
t_color	set_direct_light(t_scene scene, t_origin org, t_color color);

/*
** Glint
*/
t_color	set_glint(t_scene scene, t_origin org,
		  t_color color, t_mlx_coord pt);

/*
** Math - Vect - Matrix
*/
t_mlx_coord	set_coord(double x, double y, double z);
t_origin	set_origin(t_mlx_coord pos, t_mlx_coord rot);
t_mlx_coord	get_pt(double dist, t_origin origin);

t_mlx_coord	get_vect(t_mlx_coord p1, t_mlx_coord p2);
double		get_vect_size(t_mlx_coord vect);
double		cos_vect(t_mlx_coord v1, t_mlx_coord v2);
double		get_dot_product(t_mlx_coord vect1, t_mlx_coord vect2);
t_mlx_coord	vect_invdir(t_mlx_coord vect);

void	vect_rot(t_mlx_coord *vect, t_mlx_coord rot);
void    vect_invrot(t_mlx_coord *vect, t_mlx_coord rot);
void	matrix_rot_x(t_mlx_coord *vect, int rot);
void	matrix_rot_y(t_mlx_coord *vect, int rot);
void	matrix_rot_z(t_mlx_coord *vect, int rot);

/*
** Hook
*/
int	key_hook(int keycode, t_param *param);
void	escape(t_param *param);
int	expose_hook(t_param *param);

extern const t_tag	g_tag[];

#endif /* !RT_H_ */
