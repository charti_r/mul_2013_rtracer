/*
** constante.h for rt in /home/charti_r/rendu/MUL_2013_rtracer/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Fri Apr  4 14:32:38 2014 charti
** Last update Sun Jun  8 16:57:54 2014 chauvi_r
*/

#ifndef CONSTANTE_H_
# define CONSTANTE_H_

# define RAD(x)		((M_PI * (x)) / 180)
# define POW2(x)	((x) * (x))

/*
** Window
*/
# define WD		1200
# define HT		800
# define WIN_NAME	"RayTracer"

# define NB_CENT	25

/*
** File
*/
# define DEFAULT_FILE	"file/default.xml"

/*
** Union Color
*/
# define U_A	3
# define U_R	2
# define U_G	1
# define U_B	0

/*
** RayTracer
*/
# define DISTANCE	1024

#endif /* !CONSTANTE_H_ */
