# DOCUMENTATION #

D'après le mail envoyé récemment sur vos boîtes epitech nous serons les seuls à utiliser le parsing.
J'ai corrigé normalement tous les segfaults et fait une gestion d'erreur mais vaut mieux être prudent donc je vais vous donner quelques règles lors de la soutenance.
Ces cas sont normalement gérés je précise.

**LES 5 commandements**

1. Ne definir qu'une fois l'oeil et au début du fichier.

2. Definir les spot en dessous (autant que vous souhaitez) 

3. Les couleurs sont en héxa sous la forme 0x123456 sinon vos objets / spots seront mi automatiquement en blanc

4. glint : entre 0 et 100

5. bright :  entre 0 et 100