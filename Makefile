##
## Makefile for rt in /home/charti_r/rendu/MUL_2013_rtracer
## 
## Made by charti
## Login   <charti_r@epitech.net>
## 
## Started on  Fri Apr  4 14:24:39 2014 charti
## Last update Sun Jun  8 19:18:00 2014 chauvi_r
##

CFLAGS		+= -W -Wall -pedantic
CFLAGS		+= -I./header/ -I./header/class/ -I./Libraries/libmy/header/ -I./Libraries/libmy_mlx/header/

CC		= gcc

RM		= rm -f

NAME		= rt

LIB_DIR		= Libraries/

LIB		= -L./$(LIB_DIR)libmy -lmy -L./$(LIB_DIR)libmy_mlx -lmy_mlx
LIB		+= -lm
LIB		+= -L/usr/lib -lmlx -L/usr/lib/X11 -lXext -lX11

SRC_DIR		= source/
SRC_CLASS	= $(SRC_DIR)class/
SRC_PARSE	= $(SRC_DIR)parse/

SRC		=  $(SRC_DIR)main.c \
		   $(SRC_DIR)loadfile.c \
		   $(SRC_DIR)init.c \
		   $(SRC_DIR)math.c \
		   $(SRC_DIR)vect.c \
		   $(SRC_DIR)matrix.c \
		   $(SRC_DIR)raytracer.c \
		   $(SRC_DIR)display.c \
		   $(SRC_DIR)get_pixel_color.c \
		   $(SRC_DIR)light.c \
		   $(SRC_DIR)direct_light.c \
		   $(SRC_DIR)glint.c \
		   $(SRC_DIR)key_hook.c \
		   $(SRC_DIR)expose.c

SRC		+= $(SRC_PARSE)parse.c \
		   $(SRC_PARSE)tag.c \
		   $(SRC_PARSE)specs_tags.c \
		   $(SRC_PARSE)object.c \
		   $(SRC_PARSE)aliasing.c

SRC		+= $(SRC_CLASS)iobj/set_iobj.c \
		   $(SRC_CLASS)plane/set_plane.c \
		   $(SRC_CLASS)plane/get_dist.c \
		   $(SRC_CLASS)plane/get_normal.c \
		   $(SRC_CLASS)sphere/set_sphere.c \
		   $(SRC_CLASS)sphere/get_dist.c \
		   $(SRC_CLASS)sphere/get_normal.c \
		   $(SRC_CLASS)cylinder/set_cylinder.c \
		   $(SRC_CLASS)cylinder/get_dist.c \
		   $(SRC_CLASS)cylinder/get_normal.c \
		   $(SRC_CLASS)cone/set_cone.c \
		   $(SRC_CLASS)cone/get_dist.c \
		   $(SRC_CLASS)cone/get_normal.c

OBJ		= $(SRC:.c=.o)

all: makelib $(NAME)

makelib:
	make -C $(LIB_DIR)libmy/
	make -C $(LIB_DIR)libmy_mlx/

$(NAME): $(OBJ)
	$(CC) $(OBJ) $(LIB) -o $(NAME)

clean:
	make clean -C $(LIB_DIR)libmy/
	make clean -C $(LIB_DIR)libmy_mlx/
	$(RM) $(OBJ)

fclean:
	make fclean -C $(LIB_DIR)libmy/
	make fclean -C $(LIB_DIR)libmy_mlx/
	$(RM) $(OBJ)
	$(RM) $(NAME)

re: fclean all

.PHONY: all makelib clean fclean re
