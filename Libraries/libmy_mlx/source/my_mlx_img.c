/*
** my_mlx_init.c for my_mlx in /home/charti_r/Projet/My/my_mlx
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sun Nov 17 17:06:18 2013 charti
** Last update Fri Mar 14 00:58:29 2014 charti
*/

#include "my_mlx.h"

t_mlx_img	*my_mlx_init_img(void *mlx_ptr, int img_width, int img_height)
{
  t_mlx_img	*mlx_img;

  mlx_img = malloc(sizeof(*mlx_img));
  if (mlx_img == NULL)
    return (NULL);
  mlx_img->mlx_ptr = mlx_ptr;
  mlx_img->img_ptr = mlx_new_image(mlx_ptr, img_width, img_height);
  if (mlx_img->img_ptr == NULL)
    return (NULL);
  mlx_img->width = img_width;
  mlx_img->height = img_height;
  mlx_img->data = mlx_get_data_addr(mlx_img->img_ptr,
				   &mlx_img->bpp,
				   &mlx_img->sizeline,
				   &mlx_img->endian);
  if (mlx_img->data == NULL)
    return (NULL);
  return (mlx_img);
}

void	my_clear_image(t_mlx_img *mlx_img)
{
  int	i;

  i = -1;
  while (++i < mlx_img->sizeline * mlx_img->height)
    mlx_img->data[i] = 0;
}
