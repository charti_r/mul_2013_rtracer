/*
** my_put_pixel_img.c for my_mlx in /home/charti_r/Projet/My/my_mlx
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Fri Nov 15 19:57:59 2013 charti
** Last update Thu Mar 13 13:55:02 2014 charti
*/

#include "my_mlx.h"

void    my_pixel_put_img(t_mlx_img *mlx_img, t_mlx_coord *pt, int color)
{
  int	opp;
  int   octet;
  int   i;

  if ((pt->x >= 0 && pt->x < mlx_img->width) &&
      (pt->y >= 0 && pt->y < mlx_img->height))
    {
      opp = mlx_img->bpp >> 3;
      color = mlx_get_color_value(mlx_img->mlx_ptr, color);
      octet = (pt->x * opp) + (pt->y * mlx_img->sizeline);
      i = -1;
      while (++i < opp)
	mlx_img->data[octet + i] = (color >> (i << 3)) & 0xFF;
    }
}
