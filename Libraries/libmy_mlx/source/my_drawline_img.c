/*
** my_mlx.c for my_mlx in /home/charti_r/Projet/My/Ping_Pong
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Fri Nov 15 12:20:46 2013 charti
** Last update Tue Jan 21 05:12:45 2014 charti
*/

#include "my_mlx.h"

void		my_drawline_img(t_mlx_img *mlx_img,
				t_mlx_coord *pt1, t_mlx_coord *pt2,
				int color)
{
  t_mlx_coord	*ptc;
  int		abs;

  if (pt1->x > pt2->x)
    {
      ptc = pt1;
      pt1 = pt2;
      pt2 = ptc;
    }
  abs = pt2->y - pt1->y;
  if ((pt2->x - pt1->x) >= MY_MLX_ABS(abs))
    my_drawline_img_one(mlx_img, pt1, pt2, color);
  else
    {
      if (pt1->y < pt2->y)
	my_drawline_img_two(mlx_img, pt1, pt2, color);
      else
	my_drawline_img_three(mlx_img, pt1, pt2, color);
    }
}

void		my_drawline_img_one(t_mlx_img *mlx_img,
				    t_mlx_coord *pt1, t_mlx_coord *pt2,
				    int color)
{
  t_mlx_coord	pt;

  pt.x = pt1->x;
  while (pt.x <= pt2->x)
    {
      pt.y = pt1->y + (((pt2->y - pt1->y) * (pt.x - pt1->x)) /
		       (pt2->x - pt1->x));
      my_pixel_put_img(mlx_img, &pt, color);
      ++pt.x;
    }
}

void		my_drawline_img_two(t_mlx_img *mlx_img,
				    t_mlx_coord *pt1, t_mlx_coord *pt2,
				    int color)
{
  t_mlx_coord	pt;

  pt.y = pt1->y;
  while (pt.y <= pt2->y)
    {
      pt.x = pt1->x + (((pt2->x - pt1->x) * (pt.y - pt1->y)) /
		       (pt2->y - pt1->y));
      my_pixel_put_img(mlx_img, &pt, color);
      ++pt.y;
    }
}

void		my_drawline_img_three(t_mlx_img *mlx_img,
				      t_mlx_coord *pt1, t_mlx_coord *pt2,
				      int color)
{
  t_mlx_coord	pt;

  pt.y = pt2->y;
  while (pt.y <= pt1->y)
    {
      pt.x = pt2->x + (((pt1->x - pt2->x) * (pt.y - pt2->y)) /
		       (pt1->y - pt2->y));
      my_pixel_put_img(mlx_img, &pt, color);
      ++pt.y;
    }
}
