/*
** my_mlx_xpm.c for my_mlx in /home/charti_r/Libraries/libmy_mlx
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Jan 20 20:31:59 2014 charti
** Last update Mon Jan 20 20:42:50 2014 charti
*/

#include "my_mlx.h"

t_mlx_xpm	*my_mlx_init_xpm(void *mlx_ptr, char *xpm_file)
{
  t_mlx_xpm	*mlx_xpm;

  mlx_xpm = malloc(sizeof(*mlx_xpm));
  if (mlx_xpm == NULL)
    return (NULL);
  mlx_xpm->xpm_ptr = mlx_xpm_file_to_image
    (mlx_ptr, xpm_file, &(mlx_xpm->width), &(mlx_xpm->height));
  if (mlx_xpm->xpm_ptr == NULL)
    return (NULL);
  if (mlx_xpm->width < 0 || mlx_xpm->height < 0)
    return (NULL);
  return (mlx_xpm);
}

int	my_mlx_get_pixel_color(t_img *xpm_ptr, int x, int y)
{
  int	i;
  int	opp;
  int	octet;
  int	color;

  if ((x >= 0 && x < xpm_ptr->width) &&
      (y >= 0 && y < xpm_ptr->height))
    {
      opp = xpm_ptr->bpp >> 3;
      octet = (x * opp) + (y * xpm_ptr->size_line);
      color = 0;
      i = opp;
      while (i > 0)
        {
          --i;
          color = color + (xpm_ptr->data[octet + i] << (i << 3));
        }
      return (color);
    }
  return (0);
}
