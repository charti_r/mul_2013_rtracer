/*
** my_mlx.h for my_mlx in /home/charti_r/Projet/My/Ping_Pong
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Fri Nov 15 12:25:28 2013 charti
** Last update Mon Apr  7 16:16:20 2014 charti
*/

#ifndef MY_MLX_H_
# define MY_MLX_H_

# include <mlx.h>
# include <mlx_int.h>
# include <stdlib.h>
# include "my_mlx_define.h"

# define MY_MLX_ABS(x) ((x) < (0) ? (-(x)) : (x))

typedef struct  s_mlx_img
{
  void		*mlx_ptr;
  void		*img_ptr;
  int		width;
  int		height;
  char		*data;
  int		bpp;
  int		sizeline;
  int		endian;
}               t_mlx_img;

typedef struct	s_mlx_xpm
{
  t_img		*xpm_ptr;
  int		width;
  int		height;
}		t_mlx_xpm;

typedef struct  s_mlx_coord
{
  double	x;
  double	y;
  double	z;
}		t_mlx_coord;

/*
** mlx_img and mlx_xpm must init by their own init functions
** Colors are manipulate in hexa [0x(AA)RRGGBB]
** Some colors are all ready defined in my_mlx_define.h
*/
t_mlx_img	*my_mlx_init_img(void *mlx_ptr, int img_width, int img_height);
t_mlx_xpm	*my_mlx_init_xpm(void *mlx_ptr, char *xpm_file);
void		my_clear_image(t_mlx_img *mlx_img);

/*
** Functions to manipulates pixels in images
*/
void	my_pixel_put_img(t_mlx_img *mlx_img, t_mlx_coord *pt, int color);
int	my_mlx_get_pixel_color(t_img *xpm_ptr, int x, int y);

/*
** Function to draw lines in images
** my_drawline_img() is the only one to be used
*/
void	my_drawline_img(t_mlx_img *mlx_img,
			t_mlx_coord *pt1, t_mlx_coord *pt2,
			int color);
void    my_drawline_img_one(t_mlx_img *mlx_img,
			    t_mlx_coord *pt1, t_mlx_coord *pt2,
			    int color);
void    my_drawline_img_two(t_mlx_img *mlx_img,
			    t_mlx_coord *pt1, t_mlx_coord *pt2,
			    int color);
void    my_drawline_img_three(t_mlx_img *mlx_img,
			      t_mlx_coord *pt1, t_mlx_coord *pt2,
			      int color);

#endif /* !MY_MLX_H_ */
