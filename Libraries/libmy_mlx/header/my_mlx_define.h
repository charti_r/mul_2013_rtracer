/*
** my_mlx_define.h for my_mlx in /home/charti_r/Libraries/libmy_mlx
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sat Nov 30 02:13:36 2013 charti
** Last update Sat Apr 12 13:25:55 2014 charti
*/

#ifndef MY_MLX_DEFINE_H_
# define MY_MLX_DEFINE_H_

/*
** Colors
*/
# define MLX_WHITE	0xFFFFFF
# define MLX_SILVER	0xC0C0C0
# define MLX_GREY	0x808080
# define MLX_DIMGREY	0x404040
# define MLX_BLACK	0x000000

# define MLX_RED	0xFF0000
# define MLX_LIME	0x00FF00
# define MLX_BLUE	0x0000FF
# define MLX_MAROON	0x800000
# define MLX_GREEN	0x008000
# define MLX_NAVY	0x000080

# define MLX_YELLOW	0xFFFF00
# define MLX_MAGENTA	0xFF00FF
# define MLX_CYAN	0x00FFFF
# define MLX_OLIVE	0x808000
# define MLX_PURPLE	0x800080
# define MLX_TEAL	0x008080

# define MLX_ORANGE	0xFF8000
# define MLX_LEMON	0x80FF00
# define MLX_MINT	0x00FF80
# define MLX_ROYAL_BLUE	0x0080FF
# define MLX_PINK	0xFF0080
# define MLX_VIOLET	0x8000FF

/*
** Keys
*/
# define MLX_KEY_SPACE		0x20
# define MLX_KEY_BACKSPACE	0xFF08
# define MLX_KEY_TAB		0xFF09
# define MLX_KEY_ENTER		0xFF0D
# define MLX_KEY_ESC		0xFF1B
# define MLX_KEY_DEL		0xFFFF

# define MLX_KEY_A		0x61
# define MLX_KEY_B		0x62
# define MLX_KEY_C		0x63
# define MLX_KEY_D		0x64
# define MLX_KEY_E		0x65
# define MLX_KEY_F		0x66
# define MLX_KEY_G		0x67
# define MLX_KEY_H		0x68
# define MLX_KEY_I		0x69
# define MLX_KEY_J		0x6A
# define MLX_KEY_K		0x6B
# define MLX_KEY_L		0x6C
# define MLX_KEY_M		0x6D
# define MLX_KEY_N		0x6E
# define MLX_KEY_O		0x6F
# define MLX_KEY_P		0x70
# define MLX_KEY_Q		0x71
# define MLX_KEY_R		0x72
# define MLX_KEY_S		0x73
# define MLX_KEY_T		0x74
# define MLX_KEY_U		0x75
# define MLX_KEY_V		0x76
# define MLX_KEY_W		0x77
# define MLX_KEY_X		0x78
# define MLX_KEY_Y		0x79
# define MLX_KEY_Z		0x7A
# define MLX_KEY_LEFT		0xFF51
# define MLX_KEY_UP		0xFF52
# define MLX_KEY_RIGHT		0xFF53
# define MLX_KEY_DOWN		0xFF54

#endif /* !MY_MLX_DEFINE_H_ */
