/*
** my_sizeof_line.c for libmy in /home/charti_r/Libraries/libmy
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Dec 30 12:40:03 2013 charti
** Last update Tue Jan 21 15:02:58 2014 charti
*/

#include "my.h"

int     my_sizeof_line(char *str)
{
  int   i;

  if (!str)
    return (0);
  i = 0;
  while (str[i] != '\n' && str[i])
    ++i;
  return (i);
}
