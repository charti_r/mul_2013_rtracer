/*
** my_str_realloc.c for libmy in /home/charti_r/Libraries/libmy
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Dec 30 13:00:57 2013 charti
** Last update Fri Jun  6 11:48:03 2014 charti
*/

#include "my.h"

char	*my_str_realloc(char *str, int size)
{
  char	*new_str;

  if ((new_str = malloc(sizeof(*str) * (size + 1))) == NULL)
    return (NULL);
  if (str)
    {
      my_strcpy(new_str, str);
      free(str);
    }
  else
    new_str[0] = '\0';
  return (new_str);
}
