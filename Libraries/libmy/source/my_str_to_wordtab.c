/*
** my_str_to_wordtab.c for libmy in /home/charti_r/Libraries/libmy/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Feb 24 17:40:29 2014 charti
** Last update Mon Feb 24 17:40:30 2014 charti
*/

#include "my.h"

char	**init_str_to_wordtab(char *str, int *quote, int *count);
int	count_nb_arg(char *str);
char	*fil_arg(char *str, int count);

char	**my_str_to_wordtab(char *str)
{
  char	**argv;
  int	quote;
  int	count;
  int	i;
  int	j;

  if ((argv = init_str_to_wordtab(str, &quote, &count)) == NULL)
    return (NULL);
  j = 0;
  i = 0;
  while (str[i])
    {
      if (str[i] == '"')
	quote = !quote;
      if ((str[i] == ' ' || str[i] == '\n') && !quote)
	{
	  argv[count] = fil_arg(&str[j], i - j);
	  j = i + 1;
	  ++count;
	}
      ++i;
    }
  argv[count] = NULL;
  return (argv);
}

char	**init_str_to_wordtab(char *str, int *quote, int *count)
{
  char	**argv;

  if (!str)
    return (NULL);
  if ((argv = malloc(sizeof(*argv) * count_nb_arg(str) + 1)) == NULL)
    return (NULL);
  (*quote) = 0;
  (*count) = 0;
  return (argv);
}

int	count_nb_arg(char *str)
{
  int	i;
  int	count;
  int	quote;

  quote = 0;
  count = 1;
  i = 0;
  while (str[i])
    {
      if (str[i] == '"')
	quote = (quote == 0 ? 1 : 0);
      if ((str[i] == ' ' || str[i] == '\n') && !quote)
	++count;
      ++i;
    }
  return (count);
}

char	*fil_arg(char *str, int count)
{
  char	*arg;

  if ((arg = malloc(sizeof(*arg) * count + 1)) == NULL)
    return (NULL);
  if (str[0] == '"')
    my_strncpy(arg, &str[1], count - 2);
  else
    my_strncpy(arg, str, count);
  return (arg);
}
