/*
** my_sizeof_word.c for libmy in /home/charti_r/Libraries/libmy
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sat Mar 29 09:23:06 2014 charti
** Last update Sat Mar 29 09:25:09 2014 charti
*/

#include "my.h"

int	my_sizeof_word(char *str)
{
  int	i;

  if (!str)
    return (0);
  i = 0;
  while (str[i] != ' ' && str[i] != '\t' && str[i] != '\n' && str[i])
    ++i;
  return (i);
}
