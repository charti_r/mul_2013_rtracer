/*
** my_nbr_isneg.c for libmy in /home/charti_r/Libraries/libmy/source/math
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 17:06:08 2014 charti
** Last update Wed Feb 26 17:06:09 2014 charti
*/

#include "my.h"

int	my_nbr_isneg(char *str)
{
  int	i;
  int	isneg;

  isneg = 1;
  i = 0;
  while (str[i] == '+' || str[i] == '-')
    {
      if (str[i] == '-')
	isneg = -isneg;
      ++i;
    }
  return (isneg);
}
