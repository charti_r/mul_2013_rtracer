/*
** my_find_primeup.c for libmy in /home/charti_r/Libraries/libmy/source/math
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 17:05:08 2014 charti
** Last update Wed Feb 26 17:05:09 2014 charti
*/

#include "my.h"

int	my_find_primeup(int nb)
{
  while (!my_is_prime(nb))
    ++nb;
  return (nb);
}
