/*
** my_sizeof_char.c for libmy in /home/charti_r/Libraries/libmy/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sat Mar 29 17:28:40 2014 charti
** Last update Sat Mar 29 17:29:37 2014 charti
*/

#include "my.h"

int	my_sizeof_char(char *str, char c)
{
  int	i;

  i = 0;
  while (str[i] != c && str[i])
    ++i;
  return (i);
}
