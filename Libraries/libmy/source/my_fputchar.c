/*
** my_fputchar.c for libmy in /home/charti_r/Libraries/libmy/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Jan 22 20:21:42 2014 charti
** Last update Wed Jan 22 20:26:04 2014 charti
*/

#include "my.h"

int	my_fputchar(int fd, char c)
{
  return (write(fd, &c, 1));
}
