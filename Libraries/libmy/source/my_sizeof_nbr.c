/*
** my_sizeof_nbr.c for libmy in /home/charti_r/Libraries/libmy/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Thu Feb 27 18:17:05 2014 charti
** Last update Thu Feb 27 18:22:06 2014 charti
*/

#include "my.h"

int	my_sizeof_nbr(int nb)
{
  int	len;

  len = 0;
  while (nb)
    {
      nb /= 10;
      ++len;
    }
  return (len);
}
