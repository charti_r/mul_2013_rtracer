/*
** my_fputnbr_base.c for libmy in /home/charti_r/Libraries/libmy/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Feb 24 17:50:20 2014 charti
** Last update Mon Feb 24 17:50:51 2014 charti
*/

#include "my.h"

void     my_fputnbr_base(int fd, int nb, char *base)
{
  int   n_base;

  if (nb < 0)
    {
      my_fputchar(fd, '-');
      nb = -nb;
    }
  n_base = my_strlen(base);
  if (nb >= n_base)
    {
      my_fputnbr_base(fd, nb / n_base, base);
      my_fputnbr_base(fd, nb % n_base, base);
    }
  else
    my_fputchar(fd, base[nb]);
}
