/*
** my_revstr.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:10:07 2014 charti
** Last update Wed Feb 26 16:10:08 2014 charti
*/

#include "my.h"

char	*my_revstr(char *str)
{
  int	i;
  int	j;

  i = my_strlen(str);
  j = 0;
  while (j < i)
    my_swap_char(&str[--i], &str[j++]);
  return (str);
}
