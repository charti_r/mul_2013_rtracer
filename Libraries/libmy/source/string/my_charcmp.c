/*
** my_charcmp.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:09:54 2014 charti
** Last update Wed Feb 26 16:09:55 2014 charti
*/

#include "my.h"

int	my_charcmp(char c, char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (c == str[i])
	return (1);
      ++i;
    }
  return (0);
}
