/*
** my_strupcase.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:57:57 2014 charti
** Last update Wed Feb 26 16:57:58 2014 charti
*/

#include "my.h"

char	*my_strupcase(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] >= 'a' && str[i] <= 'z')
	str[i] = str[i] - 32;
      ++i;
    }
  return (str);
}
